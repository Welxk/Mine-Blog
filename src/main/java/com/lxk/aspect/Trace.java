package com.lxk.aspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface Trace {

    /**
     * 日志标签，用于表明阶段，方便划分流程
     * @return
     */
    String label() default "-";

    /**
     * 每次自动生成，会覆盖之前的traceId；（默认策略为有则延用，无则生成）
     * @return
     */
    boolean autoGen() default false;

}
