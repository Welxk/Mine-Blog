package com.lxk.dao;

import com.lxk.domain.TagDbEntity;
import com.lxk.domain.vo.TopNTagVo;
import com.lxk.domain.vo.TopNTypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface TagDbDao extends PageQuery<Void,TagDbEntity>{
    int insert(TagDbEntity record);

    int insertSelective(TagDbEntity record);

    List<TagDbEntity> listAll();

    TagDbEntity queryById(String id);

    void update(TagDbEntity tagDbEntity);

    List<TopNTagVo> listTopNTag(@Param("num") int num);
}