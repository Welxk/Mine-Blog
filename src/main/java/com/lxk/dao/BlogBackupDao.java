package com.lxk.dao;

import com.lxk.domain.BlogBackupEntity;
import com.lxk.domain.dto.BlogSearchQueryDto;
import com.lxk.domain.vo.IndexBlogVo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BlogBackupDao extends PageQuery<BlogSearchQueryDto, IndexBlogVo>{
    int insert(BlogBackupEntity record);

    int insertSelective(BlogBackupEntity record);

    BlogBackupEntity queryByBlogId(Long id);

    BlogBackupEntity queryById(Long id);

    void update(BlogBackupEntity record);
}