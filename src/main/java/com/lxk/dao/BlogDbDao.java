package com.lxk.dao;

import com.lxk.domain.BlogDbEntity;
import com.lxk.domain.dto.BlogQueryDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface BlogDbDao extends PageQuery<BlogQueryDto,BlogDbEntity> {
    int insert(BlogDbEntity record);

    int insertSelective(BlogDbEntity record);

    BlogDbEntity queryById(@Param("id") Long id);

    void update(BlogDbEntity blogDbEntity);

    List<BlogDbEntity> listRecommendTopN(int num);

    List<BlogDbEntity> listRecentN(int num);

    int updateByVersion(BlogDbEntity blogDbEntity);
}