package com.lxk.dao;

import com.lxk.domain.PictureDbEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PictureDao {

    int insert(PictureDbEntity record);

    int insertSelective(PictureDbEntity record);

    List<PictureDbEntity> queryAllPic();
}