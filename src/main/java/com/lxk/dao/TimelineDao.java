package com.lxk.dao;

import com.lxk.domain.BlogDbEntity;
import com.lxk.domain.TimelineDbEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TimelineDao {
    int insert(TimelineDbEntity record);

    int insertSelective(TimelineDbEntity record);

    List<TimelineDbEntity> listRecentN(int num);
}