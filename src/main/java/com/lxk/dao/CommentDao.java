package com.lxk.dao;

import com.lxk.domain.CommentDbEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CommentDao {

    int insert(CommentDbEntity record);

    int insertSelective(CommentDbEntity record);

    List<CommentDbEntity> queryByEntityIdAndType(@Param("entityId") Long entityId, @Param("entityType") Integer entityType);

    List<CommentDbEntity> queryAllComment();

    CommentDbEntity queryById(@Param("id")Long id);
}