package com.lxk.dao;

import com.lxk.domain.TypeDbEntity;
import com.lxk.domain.vo.TopNTypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TypeDbDao extends PageQuery<Void,TypeDbEntity>{

    int insert(TypeDbEntity record);

    int insertSelective(TypeDbEntity record);

    List<TypeDbEntity> listAll();

    TypeDbEntity queryById(String id);

    void update(TypeDbEntity entity);

    List<TopNTypeVo> listTopNType(@Param("num") int num);
}