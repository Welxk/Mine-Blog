package com.lxk.dao.es;
import com.lxk.domain.es.BlogEsEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * Created by Lxk on 2020/6/12.
 */
@Component
public interface BlogEsDao extends ElasticsearchRepository<BlogEsEntity,Long>{

}
