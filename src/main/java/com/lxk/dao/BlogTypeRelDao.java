package com.lxk.dao;

import com.lxk.domain.BlogTypeRelEntity;
import com.lxk.domain.TypeDbEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BlogTypeRelDao {
    int insert(BlogTypeRelEntity record);

    int insertSelective(BlogTypeRelEntity record);

    TypeDbEntity queryBlogTypeByBlogId(String blogId);
}