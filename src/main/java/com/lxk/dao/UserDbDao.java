package com.lxk.dao;

import com.lxk.domain.TagDbEntity;
import com.lxk.domain.UserDbEntity;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.vo.IndexBlogVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserDbDao extends PageQuery<BlogQueryDto,IndexBlogVo>{
    int insert(UserDbEntity record);

    int insertSelective(UserDbEntity record);

    UserDbEntity checkUser(@Param("username")String username,@Param("password")String password);

    UserDbEntity queryById(Long id);

}