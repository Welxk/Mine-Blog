package com.lxk.dao;

import com.lxk.domain.BlogTagRelEntity;
import com.lxk.domain.TagDbEntity;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.vo.IndexBlogVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BlogTagRelDao extends PageQuery<BlogQueryDto,IndexBlogVo> {
    int insert(BlogTagRelEntity record);

    int insertSelective(BlogTagRelEntity record);

    List<TagDbEntity> queryTagsByBlogId(String blogId);
}