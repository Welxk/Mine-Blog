package com.lxk.dao;

import com.lxk.domain.TagDbEntity;
import com.lxk.domain.UserDbEntity;
import com.lxk.domain.dto.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Lxk on 2020/6/15.
 */
public interface PageQuery<Q,T> {

    /**
     * 查询分页数据
     * @param pageInfo
     * @return
     */
    List<T> queryDataForPage(PageInfo<Q,T> pageInfo);

    /**
     * 查询数据总条数
     * @param pageInfo
     * @return
     */
    int queryDataTotalForPage(PageInfo<Q,T> pageInfo);

}
