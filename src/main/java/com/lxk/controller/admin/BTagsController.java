package com.lxk.controller.admin;

import com.lxk.common.Constant;
import com.lxk.domain.TagDbEntity;
import com.lxk.domain.dto.PageInfo;
import com.lxk.service.TagService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by Lxk on 2020/6/15.
 */
@Controller
@RequestMapping("/admin")
public class BTagsController {

    @Resource
    private TagService tagService;

    @GetMapping("/tags")
    public String tags(PageInfo<Void,TagDbEntity> pageInfo,Model model){
        tagService.buildPageInfo(pageInfo);
        model.addAttribute("page",pageInfo);
        return "/admin/tags";
    }


    @GetMapping("/tags/input/{id}")
    public String input(@PathVariable String id, Model model){
        if(!Constant.BConstants.NULL_VAL.equals(id)){
            TagDbEntity tagDbEntity = tagService.queryById(id);
            model.addAttribute("tag",tagDbEntity);
        }
        return "admin/tags-input";
    }

    @PostMapping("/tags/add")
    public String addType(@RequestParam String id, @RequestParam String name){
        if(StringUtils.isEmpty(id)){
            //新增
            tagService.addTag(name);
        }else{
            //修改
            TagDbEntity entity = new TagDbEntity();
            entity.setId(Long.parseLong(id));
            entity.setName(name);
            tagService.update(entity);
        }
        return "redirect:/admin/tags";
    }

    @GetMapping("/tags/delete/{id}")
    public String delete(@PathVariable String id){
        TagDbEntity entity = new TagDbEntity();
        entity.setId(Long.parseLong(id));
        entity.setYn(0);
        tagService.update(entity);
        return "redirect:/admin/tags";
    }

}
