package com.lxk.controller.admin;

import com.lxk.common.Constant;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.TypeDbEntity;
import com.lxk.service.TypeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Lxk on 2020/6/14.
 */
@Controller
@RequestMapping("/admin")
public class BTypeController {

    @Resource
    private TypeService typeService;

    @GetMapping("/types")
    public String types(PageInfo<Void,TypeDbEntity> pageInfo, Model model){
        typeService.buildPageInfo(pageInfo);
        model.addAttribute("page",pageInfo);
        return "admin/types";
    }

    @GetMapping("/types/input/{id}")
    public String input(@PathVariable String id,Model model){
        if(!Constant.BConstants.NULL_VAL.equals(id)){
            TypeDbEntity typeDbEntity = typeService.queryById(id);
            model.addAttribute("type",typeDbEntity);
        }
        return "admin/types-input";
    }

    @PostMapping("/types/add")
    public String addType(@RequestParam String id,@RequestParam String name){
        if(StringUtils.isEmpty(id)){
            //新增
            typeService.addType(name);
        }else{
            //修改
            TypeDbEntity entity = new TypeDbEntity();
            entity.setId(Long.parseLong(id));
            entity.setName(name);
            typeService.update(entity);
        }
        return "redirect:/admin/types";
    }

    @GetMapping("/types/delete/{id}")
    public String delete(@PathVariable String id){
        TypeDbEntity entity = new TypeDbEntity();
        entity.setId(Long.parseLong(id));
        entity.setYn(0);
        typeService.update(entity);
        return "redirect:/admin/types";
    }

    @ResponseBody
    @GetMapping("/types/list")
    public List<TypeDbEntity> listAll(){
        return typeService.listAll();
    }

}
