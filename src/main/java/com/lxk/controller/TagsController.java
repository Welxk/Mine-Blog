package com.lxk.controller;

import com.lxk.aspect.Trace;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;
import com.lxk.service.BlogTagRelService;
import com.lxk.service.TagService;
import com.lxk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by Lxk on 2020/6/22.
 */
@RequestMapping("/tags")
@Controller
public class TagsController {

    @Resource
    private UserService userService;

    @Resource
    private TagService tagService;

    @Resource
    private BlogTagRelService blogTagRelService;

    @GetMapping("/")
    @Trace(label = "tag")
    public String tags(PageInfo<BlogQueryDto, IndexBlogVo> pageInfo, BlogQueryDto blogQueryDto, Model model) {
        pageInfo.setQueryDto(blogQueryDto);
        if (!StringUtils.isEmpty(blogQueryDto.getTagId())) {
            blogTagRelService.buildPageInfo(pageInfo);
        } else {
            userService.buildPageInfo(pageInfo);
        }
        model.addAttribute("page", pageInfo);
        model.addAttribute("tags", tagService.listTopNTags(Integer.MAX_VALUE));
        return "/tag";
    }

}
