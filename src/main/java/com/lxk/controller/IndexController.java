package com.lxk.controller;

import com.lxk.aspect.Trace;
import com.lxk.common.Markdown2HtmlUtils;
import com.lxk.common.SwitchEnum;
import com.lxk.domain.BlogBackupEntity;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.BlogSearchQueryDto;
import com.lxk.domain.dto.CommentDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;
import com.lxk.exception.PageNotFundException;
import com.lxk.service.*;
import org.apache.lucene.store.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.HtmlUtils;

import javax.annotation.Resource;
import javax.swing.text.html.HTML;

/**
 * Created by Lxk on 2020/6/10.
 */
@Controller
public class IndexController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private UserService userService;

    @Resource
    private BlogService blogService;

    @Resource
    private TypeService typeService;

    @Resource
    private TagService tagService;

    @Resource
    private CommentService commentService;

    @GetMapping("/")
    @Trace(label = "index")
    public String index(PageInfo<BlogQueryDto,IndexBlogVo> pageInfo,BlogQueryDto blogQueryDto, Model model){
        pageInfo.setQueryDto(blogQueryDto);
        userService.buildPageInfo(pageInfo);
        model.addAttribute("page",pageInfo);
        model.addAttribute("types",typeService.listTopNTypes(6));
        model.addAttribute("tags",tagService.listTopNTags(6));
        model.addAttribute("recommends",blogService.listRecommendTopN(10));
        return "index";
    }

    @GetMapping("/blog/detail")
    @Trace(label = "blog_detail")
    public String detail(String id,Model model){
        IndexBlogVo indexBlogVo = blogService.queryById(id);
        indexBlogVo.setTitle(Markdown2HtmlUtils.markdownToHtml(indexBlogVo.getTitle()));
        indexBlogVo.setContent(Markdown2HtmlUtils.markdownToHtml(indexBlogVo.getContent()));
        if(SwitchEnum.SWITCH_OPEN.getStatus().equals(indexBlogVo.getComented())){
            model.addAttribute("comments",commentService.listComments(indexBlogVo.getId()));
        }
        model.addAttribute("blog",indexBlogVo);
        model.addAttribute("blogId",indexBlogVo.getId());
        return "/detail";
    }

    @PostMapping("/search")
    @Trace(label = "search")
    public String search(PageInfo<BlogSearchQueryDto, IndexBlogVo> pageInfo, @RequestParam String query, Model model){
        BlogSearchQueryDto queryDto = new BlogSearchQueryDto();
        queryDto.setQuery(query);
        pageInfo.setQueryDto(queryDto);
        //暂时不做分页
        pageInfo.setPageSize(Integer.MAX_VALUE);
        userService.buildSearchPageInfo(pageInfo);
        model.addAttribute("page",pageInfo);
        model.addAttribute("query",query);
        return "/search";
    }

    @GetMapping("/archives")
    public String archives(){
        return "/archives";
    }

    @PostMapping("/comment")
    @Trace(label = "comment")
    public String comment(CommentDto commentDto,Model model){
        commentService.addComment(commentDto);
        IndexBlogVo indexBlogVo = blogService.queryById(commentDto.getBlogId());
        model.addAttribute("blogId",commentDto.getBlogId());
        model.addAttribute("blog",indexBlogVo);
        model.addAttribute("comments",commentService.listComments(Long.parseLong(commentDto.getBlogId())));
        return "/detail :: comment-container";
    }

    @GetMapping("/404")
    public String errorNotFound(){
        throw new PageNotFundException("页面不存在");
    }

}
