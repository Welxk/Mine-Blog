package com.lxk.controller;

import com.lxk.aspect.Trace;
import com.lxk.dao.PictureDao;
import com.lxk.domain.PictureDbEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Lxk on 2020/7/19.
 */
@Controller
@RequestMapping("/picture")
public class PicturesController {

    @Resource
    private PictureDao pictureDao;

    @RequestMapping("/")
    @Trace(label = "pic")
    public String pictures(Model model) {
        List<PictureDbEntity> pictureInfos = pictureDao.queryAllPic();
        model.addAttribute("pictures", pictureInfos);
        return "/pictures";
    }

}
