package com.lxk.controller;

import com.lxk.aspect.Trace;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;
import com.lxk.domain.vo.TopNTypeVo;
import com.lxk.service.TypeService;
import com.lxk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Lxk on 2020/6/22.
 */
@RequestMapping("/types")
@Controller
public class TypesController {

    @Resource
    private UserService userService;

    @Resource
    private TypeService typeService;

    @GetMapping("/")
    @Trace(label = "type")
    public String types(PageInfo<BlogQueryDto,IndexBlogVo> pageInfo, BlogQueryDto blogQueryDto, Model model){
        pageInfo.setQueryDto(blogQueryDto);
        userService.buildPageInfo(pageInfo);
        model.addAttribute("page",pageInfo);
        model.addAttribute("types",typeService.listTopNTypes(Integer.MAX_VALUE));
        return "/type";
    }

}
