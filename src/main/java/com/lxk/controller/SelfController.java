package com.lxk.controller;

import com.lxk.aspect.Trace;
import com.lxk.domain.UserDbEntity;
import com.lxk.domain.vo.UserVo;
import com.lxk.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.lxk.common.Constant.BConstants.STR_USER_ID;

/**
 * Created by Lxk on 2020/6/22.
 */
@RequestMapping("/self")
@Controller
public class SelfController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    @Trace(label = "self")
    public String self(Model model) {
        UserDbEntity userDbEntity = userService.queryById(STR_USER_ID);
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(userDbEntity, userVo);
        List<String> hobbys = StringUtils.isEmpty(userDbEntity.getHobby()) ? new ArrayList<>() : Arrays.asList(userDbEntity.getHobby().split(","));
        List<String> skills = StringUtils.isEmpty(userDbEntity.getSkill()) ? new ArrayList<>() : Arrays.asList(userDbEntity.getSkill().split(","));
        userVo.setHobby(hobbys);
        userVo.setSkill(skills);
        model.addAttribute("user", userVo);
        return "/about";
    }


}
