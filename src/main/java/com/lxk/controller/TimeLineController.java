package com.lxk.controller;

import com.lxk.aspect.Trace;
import com.lxk.domain.TimelineDbEntity;
import com.lxk.service.TimelineService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Lxk on 2020/7/11.
 */
@RequestMapping("/timeline")
@Controller
public class TimeLineController {

    @Resource
    private TimelineService timelineService;

    TimelineDbEntity buildFutureTimeline() {
        TimelineDbEntity entity = new TimelineDbEntity();
        entity.setTitle("未完待续...");
        entity.setDesc("朝看水东流 暮看日西坠");
        entity.setCreateTime(new Date());
        entity.setPicInfo("https://blog-pic-1252425459.cos.ap-beijing.myqcloud.com/future.jpg");
        return entity;
    }

    @RequestMapping("/")
    @Trace(label = "timeline")
    public String timeline(Model model) {
        List<TimelineDbEntity> timelineDbEntities = new ArrayList<>();
        timelineDbEntities.add(buildFutureTimeline());
        timelineDbEntities.addAll(timelineService.recentNTimeline(Integer.MAX_VALUE));
        model.addAttribute("timelines", timelineDbEntities);
        return "/timeline";
    }

}
