package com.lxk.exception;

/**
 * Created by Lxk on 2021/1/16.
 */
public class UnloginException extends RuntimeException {

    public UnloginException(String message) {
        super(message);
    }
}
