package com.lxk.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Lxk on 2020/6/11.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class PageNotFundException extends RuntimeException {

    public PageNotFundException() {
    }

    public PageNotFundException(String message) {
        super(message);
    }

    public PageNotFundException(String message, Throwable cause) {
        super(message, cause);
    }
}
