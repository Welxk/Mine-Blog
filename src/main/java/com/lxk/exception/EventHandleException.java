package com.lxk.exception;

/**
 * Created by Lxk on 2020/7/25.
 */
public class EventHandleException extends RuntimeException {

    public EventHandleException() {
    }

    public EventHandleException(String message) {
        super(message);
    }

    public EventHandleException(String message, Throwable cause) {
        super(message, cause);
    }
}
