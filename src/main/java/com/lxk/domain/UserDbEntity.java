package com.lxk.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

import static com.lxk.common.Constant.BConstants.YN_VALID;

@ToString
@Setter
@Getter
public class UserDbEntity {

    private Long id;

    private String nickname;

    private String username;

    private String password;

    private String email;

    private String avatar;

    private Integer userType;

    private String desc;

    private String hobby;

    private String skill;

    private Date createTime;

    private Date updateTime;

    private Integer yn = YN_VALID;

}