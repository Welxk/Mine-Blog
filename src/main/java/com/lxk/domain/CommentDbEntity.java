package com.lxk.domain;

import com.lxk.common.Constant;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ToString
@Setter
@Getter
public class CommentDbEntity {

    private Long id;

    private String nickName;

    private String address;

    private String email;

    private String avatar;

    private String content;

    private Integer entityType;

    private Long entityId;

    private Integer upFlag;

    private Date createTime;

    private Date updateTime;

    private Integer yn;

    List<CommentDbEntity> replays;

    public String getAddress() {
        if(StringUtils.isBlank(address)|| Constant.BConstants.ADDR_UNKNOWN.equals(address)){
            return "未知";
        }
        return address;
    }
}