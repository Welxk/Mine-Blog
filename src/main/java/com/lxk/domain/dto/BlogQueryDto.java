package com.lxk.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.StringUtils;

/**
 * Created by Lxk on 2020/6/20.
 */
@ToString

@Getter
public class BlogQueryDto {

    @Setter
    private String title;

    @Setter
    private String blogType;

    private String typeId;

    private String tagId;

    @Setter
    private Boolean recommend;

    public void setTypeId(String typeId) {
        if(!StringUtils.isEmpty(typeId)){
            this.typeId = typeId;
        }
    }

    public void setTagId(String tagId) {
        if(!StringUtils.isEmpty(tagId)){
            this.tagId = tagId;
        }
    }

    public void setTitle(String title) {
        if(!StringUtils.isEmpty(title)){
            this.title = title;
        }
    }

    public void setBlogType(String blogType) {
        if(!StringUtils.isEmpty(blogType)){
            this.blogType = blogType;
        }
    }
}
