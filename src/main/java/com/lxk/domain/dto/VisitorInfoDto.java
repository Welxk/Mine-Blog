package com.lxk.domain.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Data
public class VisitorInfoDto {
    /**
     * 请求客户端的ip地址
     */
    private String visitIp;
    /**
     * 请求时间
     */
    private String visitTime;
    /**
     * 请求的web站点内容路径
     */
    private String visitPath;
    /**
     * 请求ip归属地省份
     */
    private String province;
    /**
     * 请求ip归属地城市
     */
    private String city;
    /**
     * 归属地城市邮编
     */
    private String adCode;
    /**
     * 请求ip的归属地左下和右上坐标
     */
    private String rectangle;

}
