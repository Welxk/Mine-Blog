package com.lxk.domain.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BlogSearchQueryDto {

    /**
     * 用户录入的查询字符串
     */
    private String query;

}
