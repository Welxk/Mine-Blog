package com.lxk.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by Lxk on 2020/6/14.
 */
@ToString
public class PageInfo<Q,T>{

    /**
     * 查询Dto对象
     */
    @Setter
    @Getter
    private Q queryDto;

    /**
     * 返回
     */
    @Setter
    @Getter
    private List<T> content;

    /**
     * 当前页
     */
    @Getter
    private Integer number = new Integer(0);

    /**
     * 数据库起始行
     */
    @Setter
    @Getter
    private Integer startRow = new Integer(0);

    /**
     * 页大小
     */
    @Setter
    @Getter
    private Integer pageSize = new Integer(5);

    /**
     * 总页数
     */
    @Getter
    private Integer pageTotal;

    /**
     * 记录总数
     */
    @Getter
    private Integer total;

    /**
     * 是否有下一页
     */
    @Setter
    private boolean next = true;

    /**
     * 是否有前一页
     */
    @Setter
    private boolean pre = true;

    public void setNumber(Integer number) {
        this.number = number;
        this.startRow = number*pageSize;
    }

    public void setTotal(Integer total) {
        this.total = total;
        this.pageTotal = total%this.pageSize==0? total/this.pageSize : total/this.pageSize + 1;
        if(this.number==0){
            this.pre = false;
        }
        if((this.number+1)==this.pageTotal){
            this.next = false;
        }
    }

    public boolean getNext(){
        return next;
    }

    public boolean getPre(){
        return pre;
    }

}
