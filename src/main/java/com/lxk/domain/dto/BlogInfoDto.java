package com.lxk.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Lxk on 2020/6/21.
 */
@ToString
@Setter
@Getter
public class BlogInfoDto {

    /**
     * 博客类型
     */
    private Integer blogType;

    /**
     * 标题
     */
    private String title;

    /**
     * 文章描述
     */
    private String desc;

    /**
     * 内容
     */
    private String content;

    /**
     * 自定义类别主键
     */
    private String typeId;

    /**
     * 标签
     */
    private String tagIds;

    /**
     * 首图信息
     */
    private String indexPicture;

    /**
     * 是否推荐
     */
    private Boolean recommend = false;

    /**
     * 是否开启维权
     */
    private Boolean shareInfo = false;

    /**
     * 是否开启赞赏
     */
    private Boolean appreciation = false;

    /**
     * 是否推荐
     */
    private Boolean commend = false;

    /**
     * 是否
     */
    private Boolean published = false;

}
