package com.lxk.domain.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Lxk on 2020/7/4.
 */
@ToString
@Setter
@Getter
public class CommentDto {

    private String blogId;

    private String commentId;

    private String toname;

    private String comment;

    private String nickname;

    private String email;

}
