package com.lxk.domain.es;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

/**
 * Created by Lxk on 2020/6/12.
 */
@ToString
@Document(indexName = "blog-index",type = "blog")
public class BlogEsEntity implements Serializable{

    /**
     * es索引
     */
    @Getter
    @Setter
    private long id;

    /**
     * 博客信息表主键
     */
    @Getter
    @Setter
    private long blogId;

    /**
     * 博客内容备份表主键
     */
    @Getter
    @Setter
    private long blogBackupId;

    @Getter
    @Setter
    private long userId;

    /**
     * 文章标题
     */
    @Getter
    @Setter
    private String title;

    /**
     * 文章简述
     */
    @Getter
    @Setter
    private String desc;

    /**
     * 文章内容
     */
    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private boolean show;

}
