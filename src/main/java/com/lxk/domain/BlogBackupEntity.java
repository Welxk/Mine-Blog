package com.lxk.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

import static com.lxk.common.Constant.BConstants.YN_VALID;

@ToString
@Setter
@Getter
public class BlogBackupEntity {
    private Long id;

    private Long blogId;

    private String title;
    private String content;
    private Date createTime;

    private Date updateTime;

    private Integer yn  = YN_VALID;
}