package com.lxk.domain.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

import static com.lxk.common.Constant.BConstants.YN_VALID;

/**
 * Created by Lxk on 2020/6/26.
 */
@ToString
@Setter
@Getter
public class UserVo {

    private Long id;

    private String nickname;

    private String username;

    private String password;

    private String email;

    private String avatar;

    private Integer userType;

    private String desc;

    private List<String> hobby;

    private List<String> skill;

    private Date createTime;

    private Date updateTime;

    private Integer yn = YN_VALID;
}
