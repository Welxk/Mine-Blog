package com.lxk.domain.vo;

import com.lxk.domain.TagDbEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.lxk.common.Constant.BConstants.YN_VALID;

/**
 * Created by Lxk on 2020/6/22.
 */
@ToString
@Setter
@Getter
public class IndexBlogVo {

    private Long id;

    private Long esId;

    private String title;

    private String desc;

    private String content;

    private String firstPicInfo;

    private String username;

    private String avatar;

    private String typeName;

    List<TagDbEntity> tags = new ArrayList<>();

    private Integer blogType;

    private Integer viewCount;

    private Integer appreciation;

    private Integer shareStatement;

    private Integer comented;

    private Integer published;

    private Integer recommend;

    private Date createTime;

    private Date updateTime;

    private Integer yn = YN_VALID;


}
