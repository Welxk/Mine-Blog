package com.lxk.domain.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Lxk on 2020/6/22.
 */
@ToString
@Setter
@Getter
public class TopNTypeVo {

    private String id;

    private String name;

    private Integer count;

}
