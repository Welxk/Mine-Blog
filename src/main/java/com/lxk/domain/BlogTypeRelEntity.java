package com.lxk.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

import static com.lxk.common.Constant.BConstants.YN_VALID;

@ToString
@Setter
@Getter
public class BlogTypeRelEntity {
    private Long id;

    private Long esBlogId;

    private Long dbBlogId;

    private Long dbTypeId;

    private String typeName;

    private Date createTime;

    private Date updateTime;

    private Integer yn = YN_VALID;;

}