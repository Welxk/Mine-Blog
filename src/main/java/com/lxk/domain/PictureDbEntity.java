package com.lxk.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ToString
@Setter
@Getter
public class PictureDbEntity {

    private Long id;

    private String picInfo;

    private String picDesc;

    private Date createTime;

    private Date updateTime;

    private Integer yn;

}