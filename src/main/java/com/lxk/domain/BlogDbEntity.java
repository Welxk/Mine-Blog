package com.lxk.domain;

import com.lxk.domain.vo.IndexBlogVo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.cglib.beans.BeanCopier;

import java.util.Date;

import static com.lxk.common.Constant.BConstants.YN_VALID;

@ToString
@Setter
@Getter
public class BlogDbEntity {

    private Long id;

    private Long esId;

    private Long backupId;

    private String title;

    private String desc;

    private Long userId;

    private String firstPicInfo;

    private Long typeId;

    private String typeName;

    private Integer blogType;

    private Integer viewCount;

    private Integer appreciation;

    private Integer shareStatement;

    private Integer comented;

    private Integer published;

    private Integer recommend;

    private Date createTime;

    private Date updateTime;

    private Integer yn = YN_VALID;

    public static IndexBlogVo convertBlogVo(BlogDbEntity blogDbEntity){
        IndexBlogVo vo = new IndexBlogVo();
        BeanCopier beanCopier = BeanCopier.create(BlogDbEntity.class,IndexBlogVo.class,false);
        beanCopier.copy(blogDbEntity,vo,null);
        return vo;
    }

}