package com.lxk.utils;

import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import java.util.UUID;

public class TraceUtil {
    private static final String TRACE_ID = "traceId";

    private static final String LABEL = "label";
    public static void genTrace(String label){
        MDC.put(LABEL,label);
        if(StringUtils.isEmpty(MDC.get(TRACE_ID))){
            String traceId = UUID.randomUUID().toString().replace("-","");
            MDC.put(TRACE_ID,traceId);
        }
    }

    public static void cleanTrace(){
        if(!StringUtils.isEmpty(MDC.get(TRACE_ID))){
            MDC.remove(TRACE_ID);
        }
        if(!StringUtils.isEmpty(MDC.get(LABEL))){
            MDC.remove(LABEL);
        }
    }
}
