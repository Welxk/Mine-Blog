package com.lxk.utils;

import com.lxk.aspect.RequestInfo;

public class RequestUtils {

    private static ThreadLocal<RequestInfo> requestInfoTheadLocal = new ThreadLocal<>();

    public static RequestInfo getRequestInfo(){
        return requestInfoTheadLocal.get();
    }

    public static void putRequestInfo(RequestInfo requestInfo){
        requestInfoTheadLocal.set(requestInfo);
    }

    public static void removeRequestInfo(){
        requestInfoTheadLocal.remove();
    }

}
