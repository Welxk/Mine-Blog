package com.lxk.utils;

import org.apache.commons.lang3.StringUtils;

public class RedisNamingUtils {

    private static final String IP_GEO_PREFIX = "ip_geo-";

    /**
     * ip地址转物理地址，redis存储key统一命名
     * @param ip
     * @return
     */
    public static String genIpGeoKey(String ip){
        if(StringUtils.isBlank(ip)){
            throw new RuntimeException("ip is null");
        }
        return IP_GEO_PREFIX+ip;
    }

}
