package com.lxk.utils;
import ch.qos.logback.classic.PatternLayout;

/**
 * @version 1.0
 * @Project: enterprise-credit
 * @Title:
 * @Description:
 * @Date: 2021/1/13 14:20
 * @Company: ByteDance
 * @Copyright: Copyright (c) 2021
 * @author: lixiaokun.roy@bytedance.com
 **/
public class ConvertPatternLayout extends PatternLayout {

    public static String PATTERN = "%level %d{yyyy-MM-dd HH:mm:ss.SSS} %X{LOG_VERSION:-v1}\\(%X{NUM_HEADERS:-6}\\) %F:%L %X{HOST_IP:-_} %X{PSM:-_} %X{LOG_ID:-_} %X{CLUSTER:-_} %X{STAGE:-_} [%t] %msg%n%wEx";

    static {
        defaultConverterMap.put("msg", SensitiveLogConvert.class.getName());
    }

    public static void init(){
        System.setProperty("logging.pattern.file","%level %d{yyyy-MM-dd HH:mm:ss.SSS} %X{LOG_VERSION:-v1}\\(%X{NUM_HEADERS:-6}\\) %F:%L %X{HOST_IP:-_} %X{PSM:-_} %X{LOG_ID:-_} %X{CLUSTER:-_} %X{STAGE:-_} [%t] %msg%n%wEx");
    }
}