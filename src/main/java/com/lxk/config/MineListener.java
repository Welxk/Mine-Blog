package com.lxk.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * Created by Lxk on 2020/6/18.
 */
@WebListener
public class MineListener implements ServletContextListener {

    private static final Logger logger = LoggerFactory
            .getLogger(MineListener.class);

    private Driver driver = null;
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("register......");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                logger.info("deregistering jdbc driver: {}", driver);
            } catch (SQLException e) {
                logger.info("Error deregistering driver {}", driver);
            }
        }
    }
}
