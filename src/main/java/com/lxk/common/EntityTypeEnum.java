package com.lxk.common;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Lxk on 2020/7/5.
 */
public enum EntityTypeEnum {
    ENTITY_TYPE_BLOG(0,"博客类型"),
    ENTITY_TYPE_COMMENT(1,"评论类型")
    ;
    /**
     * 实体类型编码
     */
    @Getter
    private Integer code;
    /**
     * 实体类型说明
     */
    @Getter
    private String desc;

    EntityTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static EntityTypeEnum getEntityTypeByCode(Integer code){
        if(code==null){
            return null;
        }
        Optional<EntityTypeEnum> entityTypeEnumOptional = Arrays.stream(EntityTypeEnum.values()).filter(each -> !each.getCode().equals(code)).findFirst();
        return entityTypeEnumOptional.isPresent()?entityTypeEnumOptional.get():null;
    }

}
