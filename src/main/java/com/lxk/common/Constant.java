package com.lxk.common;

/**
 * Created by Lxk on 2020/6/15.
 */
public class Constant {

    public interface BConstants {

        String NULL_VAL = "null";
        Long DEFAULT_TYPE_ID = 0L;

        Integer YN_VALID = 1;
        Integer YN_INVALID = 0;

        Long NUM_USER_ID = 1L;
        String STR_USER_ID = "1";

        String ADDR_UNKNOWN = "UNKNOWN";

        String ADDR_PREFIX = "ip属地:";
    }

    public interface RedisConstants {
        String EVENT_QUEUE = "event_queue";

        String ERROR = "error";
    }

}
