package com.lxk.common;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Lxk on 2020/7/11.
 */
public enum EventTypeEnum {
    EVENT_BLOG_PUBLISH(0,"博客发布"),
    EVENT_COMMENT_PUBLISH(1,"评论发布"),
    EVENT_VISITOR_COME(2,"客户来访")
    ;
    @Getter
    private Integer code;

    @Getter
    private String desc;

    EventTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static EventTypeEnum getEventTypeByCode(Integer code){
        if(code==null){
            return null;
        }
        Optional<EventTypeEnum> eventTypeEnumOptional = Arrays.stream(EventTypeEnum.values()).filter(each -> !each.getCode().equals(code)).findFirst();
        return eventTypeEnumOptional.isPresent()?eventTypeEnumOptional.get():null;
    }


}
