package com.lxk.common;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Lxk on 2020/7/11.
 */
public enum UpFlagEnum {
    FLAG_NORMAL(0,"正常"),
    FLAG_UP(1,"置顶")
    ;
    @Getter
    private Integer code;

    @Getter
    private String desc;

    UpFlagEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static UpFlagEnum getUpFlagByCode(Integer code){
        if(code == null){
            return null;
        }
        Optional<UpFlagEnum> upFlagEnumOptional = Arrays.stream(UpFlagEnum.values()).filter(each -> !each.getCode().equals(code)).findFirst();
        return upFlagEnumOptional.isPresent()?upFlagEnumOptional.get():null;
    }
}
