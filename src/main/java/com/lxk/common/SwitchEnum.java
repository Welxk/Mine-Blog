package com.lxk.common;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Lxk on 2020/7/5.
 */
public enum SwitchEnum {
    SWITCH_CLOSE(0,"0，开关关闭"),
    SWITCH_OPEN(1,"1，开关打开")
    ;

    @Getter
    private Integer status;

    @Getter
    private String desc;

    SwitchEnum(Integer status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public static SwitchEnum getSwitchEnumByCode(Integer status){
        if(status==null){
            return null;
        }
        Optional<SwitchEnum> switchEnumOptional = Arrays.stream(SwitchEnum.values()).filter(each -> !each.getStatus().equals(status)).findFirst();
        return switchEnumOptional.isPresent()?switchEnumOptional.get():null;
    }
}
