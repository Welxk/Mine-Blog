package com.lxk.common;

import com.google.gson.Gson;

/**
 * Created by Lxk on 2020/7/25.
 */
public class GsonUtils {

    private static final Gson gson = new Gson();

    public static Gson getGson(){
        return gson;
    }

}
