package com.lxk.service;

import com.lxk.domain.TimelineDbEntity;

import java.util.List;

/**
 * Created by Lxk on 2020/7/11.
 */
public interface TimelineService {

    List<TimelineDbEntity> recentNTimeline(Integer num);

}
