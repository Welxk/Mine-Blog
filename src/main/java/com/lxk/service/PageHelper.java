package com.lxk.service;

import com.lxk.dao.BlogBackupDao;
import com.lxk.dao.PageQuery;
import com.lxk.domain.dto.BlogSearchQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;

/**
 * Created by Lxk on 2020/6/15.
 */
public class PageHelper {

    public static  <Q,T> void queryByPageInfo(PageInfo<Q,T> pageInfo, PageQuery<Q,T> pageQuery){
        pageInfo.setTotal(pageQuery.queryDataTotalForPage(pageInfo));
        pageInfo.setContent(pageQuery.queryDataForPage(pageInfo));
    }

}
