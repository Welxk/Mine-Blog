package com.lxk.service;

import com.lxk.dao.CommentDao;
import com.lxk.domain.CommentDbEntity;
import com.lxk.domain.dto.CommentDto;

import java.util.List;

/**
 * Created by Lxk on 2020/7/5.
 */
public interface CommentService {

    /**
     * 增加评论
     * @param commentDto
     */
    void addComment(CommentDto commentDto);

    /**
     * 列出某个博客下所有的评论
     * @param blogId
     * @return
     */
    List<CommentDbEntity> listComments(Long blogId);

}
