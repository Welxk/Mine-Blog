package com.lxk.service;

import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.TypeDbEntity;
import com.lxk.domain.vo.TopNTypeVo;

import java.util.List;

/**
 * Created by Lxk on 2020/6/14.
 */
public interface TypeService {

    void buildPageInfo(PageInfo pageInfo);

    void addType(String typeName);

    TypeDbEntity queryById(String id);

    void update(TypeDbEntity entity);

    List<TypeDbEntity> listAll();

    List<TopNTypeVo> listTopNTypes(Integer num);
}
