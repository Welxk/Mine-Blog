package com.lxk.service.event;

import com.google.common.eventbus.Subscribe;
import com.lxk.exception.EventHandleException;
import com.lxk.service.event.domain.BaseEvent;

/**
 * Created by Lxk on 2020/7/25.
 */
public abstract class AbstractEventHandler<T extends BaseEvent> implements EventHandler<T> {

    @Subscribe
    public void dispatchEvent(T event) {
        //事件是否能够处理
        if (!canHandle(event)) {
            return;
        }
        boolean res = handleEvent(event);
        //如果事件处理失败，并且需要进行后续重试处理
        if (!res && isFailedRetry()) {
            throw new EventHandleException(String.format("%s事件处理异常", event.getEventType().getDesc()));
        }
    }

    boolean canHandle(T event) {
        return supports().contains(event.getEventType());
    }

}
