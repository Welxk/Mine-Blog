package com.lxk.service.event;

import com.lxk.common.EventTypeEnum;
import com.lxk.service.event.domain.EventDto;

import java.util.List;

/**
 * Created by Lxk on 2020/7/11.
 */
public interface EventHandler<T> {

    List<EventTypeEnum> supports();

    boolean handleEvent(T eventDto);

    boolean isFailedRetry();

    Class<T> eventType();

}
