package com.lxk.service.event.domain;

import com.lxk.common.EventTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Lxk on 2020/7/25.
 */
@ToString
public class BaseEvent {

    @Setter
    @Getter
    private EventTypeEnum eventType;

}
