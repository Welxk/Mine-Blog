package com.lxk.service.event.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lxk on 2020/7/25.
 */
public class OtherEventDto extends BaseEvent {

    @Getter
    @Setter
    private String content;

}
