package com.lxk.service.event.domain;

import lombok.Data;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * Created by Lxk on 2020/8/8.
 */
@Data
public class EventHandleMethodUnit implements Serializable {

    /**
     * 事件上下文
     */
    private String eventContext;
    /**
     * 方法名称
     */
    private String methodName;
    /**
     * 执行实例名称
     */
    private String beanName;
    /**
     * 实例
     */
    private Object bean;
}
