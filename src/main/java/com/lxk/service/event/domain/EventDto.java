package com.lxk.service.event.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.MDC;

/**
 * Created by Lxk on 2020/7/25.
 */
@ToString
public class EventDto extends BaseEvent{

    @Getter
    @Setter
    private String content;

}
