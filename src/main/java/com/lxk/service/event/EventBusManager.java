package com.lxk.service.event;

import com.google.common.eventbus.EventBus;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Lxk on 2020/7/25.
 */
@Component
public class EventBusManager {

    private Map<EventBusEnum,EventBus> eventBusMap = new ConcurrentHashMap<>();

    public void put(EventBusEnum eventBusEnum,EventBus eventBus){
        if(eventBusMap.keySet().contains(eventBusEnum)){
            throw new IllegalStateException("Multiple EventBusEnum conflict");
        }else{
            synchronized (this){
                if(!eventBusMap.containsKey(eventBusEnum)){
                    eventBusMap.put(eventBusEnum,eventBus);
                }
            }
        }
    }

    public EventBus get(EventBusEnum eventBusEnum){
        return eventBusMap.get(eventBusEnum);
    }

}
