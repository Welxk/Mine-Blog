package com.lxk.service.event.handler;

import com.lxk.common.EventTypeEnum;
import com.lxk.service.event.AbstractEventHandler;
import com.lxk.service.event.domain.OtherEventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Lxk on 2020/7/25.
 */
@Component
public class OtherEventHandler extends AbstractEventHandler<OtherEventDto> {

    private static final Logger logger = LoggerFactory.getLogger(OtherEventHandler.class);

    @Override
    public List<EventTypeEnum> supports() {
        return Arrays.asList(EventTypeEnum.EVENT_BLOG_PUBLISH);
    }

    @Override
    public boolean handleEvent(OtherEventDto eventDto) {
        logger.info("OtherEventHandler.handleEvent()");
        return false;
    }

    @Override
    public boolean isFailedRetry() {
        return false;
    }

    @Override
    public Class<OtherEventDto> eventType() {
        return OtherEventDto.class;
    }
}
