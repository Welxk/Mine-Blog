package com.lxk.service.event;

import lombok.Getter;

/**
 * Created by Lxk on 2020/7/25.
 */
public enum EventBusEnum {

    EVENT_BUS_PUBLISH("publish-event-bus","发布事件处理总线")
    ;
    @Getter
    private String eventBusName;

    @Getter
    private String eventBusDesc;

    EventBusEnum(String eventBusName, String eventBusDesc) {
        this.eventBusName = eventBusName;
        this.eventBusDesc = eventBusDesc;
    }
}
