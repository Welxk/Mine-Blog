package com.lxk.service;

import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;

/**
 * Created by Lxk on 2020/6/26.
 */
public interface BlogTagRelService {

    void buildPageInfo(PageInfo<BlogQueryDto,IndexBlogVo> pageInfo);

}
