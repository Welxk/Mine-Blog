package com.lxk.service;

import com.lxk.domain.BlogDbEntity;
import com.lxk.domain.dto.BlogInfoDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;

import java.util.List;

/**
 * Created by Lxk on 2020/6/16.
 */
public interface BlogService {

    void buildPageInfo(PageInfo pageInfo);

    void saveBlog(BlogInfoDto blogInfoDto);

    IndexBlogVo queryById(String id);

    void update(BlogDbEntity blogDbEntity);

    void deleteBlog(Long Id);

    List<BlogDbEntity> listRecommendTopN(Integer num);

    List<BlogDbEntity> listRecentN(Integer num);

}
