package com.lxk.service.component;

import com.lxk.common.Constant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Protocol;

/**
 * Created by Lxk on 2020/8/8.
 */
@Slf4j
@Component
public class RedisClient implements InitializingBean{

    @Value("${redis.hostname}")
    private String hostName;

    @Value("${redis.port}")
    private Integer port;

    private static JedisPool jedisPool = null;

    public static Jedis getJedisClient(){
        return jedisPool.getResource();
    }

    /**
     * 队列入队元素
     * @param key
     * @param value
     * @return
     */
    public boolean rpush(String key,String value){
        Jedis client = jedisPool.getResource();
        try{
            long res = client.rpush(key,value);
            return res>0;
        }catch (Exception e){
            log.error("redis rpush error :: key:{},value:{},e:",key,value,e);
        }finally {
            if(!ObjectUtils.isEmpty(client)){
                client.close();
            }
        }
        return false;
    }

    /**
     * 队列元素出队
     * @param key
     * @return
     */
    public String lpop(String key){
        Jedis client = jedisPool.getResource();
        try{
            return client.lpop(key);
        }catch (Exception e){
            log.error("redis lpop error :: key:{},e",key,e);
        }finally {
            if(!ObjectUtils.isEmpty(client)){
                client.close();
            }
        }
        return Constant.RedisConstants.ERROR;
    }


    public boolean setex(String key,String value,Integer expireTimeSecond){
        Jedis client = jedisPool.getResource();
        try{
            return client.setex(key,expireTimeSecond,value)!=null;
        }catch (Exception e){
            log.error("redis setex error :: key:{},value:{},e:",key,value,e);
        }finally {
            if(!ObjectUtils.isEmpty(client)){
                client.close();
            }
        }
        return false;
    }

    public boolean setex(String key,String value){
        Jedis client = jedisPool.getResource();
        try{
            return client.setex(key,60*5,value)!=null;
        }catch (Exception e){
            log.error("redis setex error :: key:{},value:{},e:",key,value,e);
        }finally {
            if(!ObjectUtils.isEmpty(client)){
                client.close();
            }
        }
        return false;
    }

    public String getKey(String key){
        Jedis jedis = jedisPool.getResource();
        try{
            return jedis.get(key);
        }catch (Exception e){
            log.error("redis getKey error :: key:{},e:",key,e);
        }finally {
            if(!ObjectUtils.isEmpty(jedis)){
                jedis.close();
            }
        }
        return Constant.RedisConstants.ERROR;
    }

    boolean exist(String key){
        Jedis jedis = jedisPool.getResource();
        try{
            return jedis.exists(key);
        }catch (Exception e){
            log.error("redis exist error :: key:{},e:",key,e);
        }finally {
            if(!ObjectUtils.isEmpty(jedis)){
                jedis.close();
            }
        }
        throw new RuntimeException("redis exist error");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxIdle(10);
        poolConfig.setMinIdle(0);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMaxWaitMillis(3000);
        jedisPool = new JedisPool(poolConfig, hostName, port, 2000, null, Protocol.DEFAULT_DATABASE);
    }
}
