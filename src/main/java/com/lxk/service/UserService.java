package com.lxk.service;

import com.lxk.domain.BlogBackupEntity;
import com.lxk.domain.UserDbEntity;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.BlogSearchQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;

/**
 * Created by Lxk on 2020/6/13.
 */
public interface UserService {

    void buildPageInfo(PageInfo<BlogQueryDto,IndexBlogVo> pageInfo);

    void buildSearchPageInfo(PageInfo<BlogSearchQueryDto, IndexBlogVo> pageInfo);

    public UserDbEntity checkUser(String userName, String password);

    public void addUser(UserDbEntity userDbEntity);

    public UserDbEntity queryById(String id);

}
