package com.lxk.service;

import com.lxk.domain.TagDbEntity;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.TopNTagVo;
import com.lxk.domain.vo.TopNTypeVo;

import java.util.List;

/**
 * Created by Lxk on 2020/6/21.
 */
public interface TagService {

    List<TagDbEntity> listAll();

    void buildPageInfo(PageInfo pageInfo);

    TagDbEntity queryById(String id);

    void addTag(String tagName);

    void update(TagDbEntity tagDbEntity);

    List<TopNTagVo> listTopNTags(Integer num);
}
