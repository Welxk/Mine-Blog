package com.lxk.service.impl;

import com.lxk.dao.TagDbDao;
import com.lxk.domain.TagDbEntity;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.TopNTagVo;
import com.lxk.service.PageHelper;
import com.lxk.service.TagService;
import com.lxk.service.utils.SnowflakeIdWorker;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.lxk.common.Constant.BConstants.YN_VALID;

/**
 * Created by Lxk on 2020/6/21.
 */
@Service
public class TagServiceImpl implements TagService {

    @Resource
    private TagDbDao tagDbDao;

    @Resource
    private SnowflakeIdWorker snowflakeIdWorker;

    @Override
    public List<TagDbEntity> listAll() {
        return tagDbDao.listAll();
    }

    @Override
    public void buildPageInfo(PageInfo pageInfo) {
        PageHelper.queryByPageInfo(pageInfo,tagDbDao);
    }

    @Override
    public TagDbEntity queryById(String id) {
        return tagDbDao.queryById(id);
    }

    @Override
    public void addTag(String tagName) {
        TagDbEntity tagDbEntity = new TagDbEntity();
        tagDbEntity.setId(snowflakeIdWorker.nextId());
        tagDbEntity.setName(tagName);
        tagDbEntity.setCreateTime(new Date());
        tagDbEntity.setUpdateTime(new Date());
        tagDbEntity.setYn(YN_VALID);
        tagDbDao.insert(tagDbEntity);
    }

    @Override
    public void update(TagDbEntity tagDbEntity) {
        tagDbDao.update(tagDbEntity);
    }

    @Override
    public List<TopNTagVo> listTopNTags(Integer num) {
        return tagDbDao.listTopNTag(num);
    }
}
