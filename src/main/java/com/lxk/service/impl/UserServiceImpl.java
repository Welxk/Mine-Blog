package com.lxk.service.impl;

import com.lxk.dao.BlogBackupDao;
import com.lxk.dao.BlogDbDao;
import com.lxk.dao.BlogTagRelDao;
import com.lxk.dao.UserDbDao;
import com.lxk.domain.BlogBackupEntity;
import com.lxk.domain.BlogDbEntity;
import com.lxk.domain.UserDbEntity;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.BlogSearchQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;
import com.lxk.service.PageHelper;
import com.lxk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Lxk on 2020/6/13.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDbDao userDbDao;

    @Autowired
    private BlogTagRelDao blogTagRelDao;

    @Autowired
    private BlogBackupDao blogBackupDao;

    @Autowired
    private BlogDbDao blogDbDao;

    @Override
    public void buildPageInfo(PageInfo<BlogQueryDto,IndexBlogVo> pageInfo) {
        PageHelper.queryByPageInfo(pageInfo,userDbDao);
        if(!CollectionUtils.isEmpty(pageInfo.getContent())){
            for(IndexBlogVo vo : pageInfo.getContent()){
                vo.setTags(blogTagRelDao.queryTagsByBlogId(String.valueOf(vo.getId())));
            }
        }
    }

    @Override
    public void buildSearchPageInfo(PageInfo<BlogSearchQueryDto, IndexBlogVo> pageInfo) {
        PageHelper.queryByPageInfo(pageInfo,blogBackupDao);
        if(!CollectionUtils.isEmpty(pageInfo.getContent())){
            for(IndexBlogVo vo : pageInfo.getContent()){
                vo.setTags(blogTagRelDao.queryTagsByBlogId(String.valueOf(vo.getId())));
            }
        }
    }

    @Override
    public UserDbEntity checkUser(String username, String password) {
        return userDbDao.checkUser(username,password);
    }

    @Override
    public void addUser(UserDbEntity userDbEntity) {
        userDbDao.insert(userDbEntity);
    }

    @Override
    public UserDbEntity queryById(String id) {
        return userDbDao.queryById(Long.parseLong(id));
    }


}
