package com.lxk.service.impl;

import com.lxk.dao.BlogTagRelDao;
import com.lxk.domain.dto.BlogQueryDto;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.vo.IndexBlogVo;
import com.lxk.service.BlogTagRelService;
import com.lxk.service.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * Created by Lxk on 2020/6/26.
 */
@Service
public class BlogTagRelServiceImpl implements BlogTagRelService {

    @Autowired
    private BlogTagRelDao blogTagRelDao;

    @Override
    public void buildPageInfo(PageInfo<BlogQueryDto, IndexBlogVo> pageInfo) {
        PageHelper.queryByPageInfo(pageInfo,blogTagRelDao);
        if(!CollectionUtils.isEmpty(pageInfo.getContent())){
            for(IndexBlogVo vo : pageInfo.getContent()){
                vo.setTags(blogTagRelDao.queryTagsByBlogId(String.valueOf(vo.getId())));
            }
        }
    }
}
