package com.lxk.service.impl;

import com.lxk.dao.TimelineDao;
import com.lxk.domain.TimelineDbEntity;
import com.lxk.service.TimelineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Lxk on 2020/7/11.
 */
@Service
public class TimelineServiceImpl implements TimelineService {

    @Autowired
    private TimelineDao timelineDao;

    @Override
    public List<TimelineDbEntity> recentNTimeline(Integer num) {
        return timelineDao.listRecentN(num);
    }
}
