package com.lxk.service.impl;

import com.lxk.dao.TypeDbDao;
import com.lxk.domain.dto.PageInfo;
import com.lxk.domain.TypeDbEntity;
import com.lxk.domain.vo.TopNTypeVo;
import com.lxk.service.PageHelper;
import com.lxk.service.TypeService;
import com.lxk.service.utils.SnowflakeIdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static com.lxk.common.Constant.BConstants.YN_VALID;

/**
 * Created by Lxk on 2020/6/14.
 */
@Service
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeDbDao typeDbDao;

    @Resource
    private SnowflakeIdWorker snowflakeIdWorker;

    @Override
    public void buildPageInfo(PageInfo pageInfo) {
        PageHelper.queryByPageInfo(pageInfo,typeDbDao);
    }

    @Override
    public void addType(String typeName) {
        TypeDbEntity typeDbEntity = new TypeDbEntity();
        typeDbEntity.setId(snowflakeIdWorker.nextId());
        typeDbEntity.setName(typeName);
        typeDbEntity.setCreateTime(new Date());
        typeDbEntity.setUpdateTime(new Date());
        typeDbEntity.setYn(YN_VALID);
        typeDbDao.insert(typeDbEntity);
    }

    @Override
    public TypeDbEntity queryById(String id) {
        return typeDbDao.queryById(id);
    }

    @Override
    public void update(TypeDbEntity entity) {
        typeDbDao.update(entity);
    }

    @Override
    public List<TypeDbEntity> listAll() {
        return typeDbDao.listAll();
    }

    @Override
    public List<TopNTypeVo> listTopNTypes(Integer num) {
        return typeDbDao.listTopNType(num);
    }
}
