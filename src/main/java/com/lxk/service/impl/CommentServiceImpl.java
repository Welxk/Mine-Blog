package com.lxk.service.impl;

import com.lxk.common.*;
import com.lxk.dao.CommentDao;
import com.lxk.domain.CommentDbEntity;
import com.lxk.domain.dto.CommentDto;
import com.lxk.service.CommentService;
import com.lxk.service.component.RedisClient;
import com.lxk.service.event.EventBusEnum;
import com.lxk.service.event.EventBusManager;
import com.lxk.service.event.domain.EventDto;
import com.lxk.service.outer.GeoService;
import com.lxk.service.utils.SensitiveWordsHandler;
import com.lxk.utils.RedisNamingUtils;
import com.lxk.utils.RequestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;

import java.util.List;

import static com.lxk.common.Constant.BConstants.YN_VALID;

/**
 * Created by Lxk on 2020/7/5.
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Resource
    private CommentDao commentDao;

    @Resource
    private EventBusManager eventBusManager;

    @Resource
    private SensitiveWordsHandler sensitiveWordsHandler;

    @Resource
    private GeoService geoService;

    @Override
    public void addComment(CommentDto commentDto) {
        CommentDbEntity commentDbEntity = new CommentDbEntity();

        if(StringUtils.isNotBlank(commentDto.getToname())){
            //回复评论
            commentDbEntity.setEntityType(EntityTypeEnum.ENTITY_TYPE_COMMENT.getCode());
            commentDbEntity.setEntityId(Long.parseLong(commentDto.getCommentId()));
        }else{
            //回复博客
            commentDbEntity.setEntityType(EntityTypeEnum.ENTITY_TYPE_BLOG.getCode());
            commentDbEntity.setEntityId(Long.parseLong(commentDto.getBlogId()));
        }
        commentDbEntity.setNickName(commentDto.getNickname());
        String address = Constant.BConstants.ADDR_UNKNOWN;
        //设置物理地址
        if(!ObjectUtils.isEmpty(RequestUtils.getRequestInfo())
                &&StringUtils.isNotBlank(RequestUtils.getRequestInfo().getIp())){
            GeoService.GeoResponse geoResponse = geoService.fromIp2Addr(RequestUtils.getRequestInfo().getIp());
            address = !ObjectUtils.isEmpty(geoResponse)&&StringUtils.isNotBlank(geoResponse.getProvince())?
                    geoResponse.getProvince() : Constant.BConstants.ADDR_UNKNOWN;
        }
        commentDbEntity.setAddress(address);
        commentDbEntity.setEmail(commentDto.getEmail());
        int code =  commentDto.getEmail().hashCode()%10;
        commentDbEntity.setAvatar("/image/header-pic/pic_"+ Math.abs(code) +".jpg");
        commentDbEntity.setContent(commentDto.getComment());
        commentDbEntity.setUpFlag(UpFlagEnum.FLAG_NORMAL.getCode());
        commentDbEntity.setYn(YN_VALID);
        commentDbEntity.getReplays();
        commentDao.insert(commentDbEntity);
        EventDto eventDto = new EventDto();
        eventDto.setEventType(EventTypeEnum.EVENT_COMMENT_PUBLISH);
        eventDto.setContent(GsonUtils.getGson().toJson(commentDto));
        eventBusManager.get(EventBusEnum.EVENT_BUS_PUBLISH).post(eventDto);
    }

    @Override
    public List<CommentDbEntity> listComments(Long blogId) {
        List<CommentDbEntity> commentDbEntityList = commentDao.queryByEntityIdAndType(blogId,EntityTypeEnum.ENTITY_TYPE_BLOG.getCode());
        //敏感信息进行脱敏
        commentDbEntityList.stream().forEach(each->{
            each.setContent(sensitiveWordsHandler.filter(each.getContent()));
            each.setAddress(Constant.BConstants.ADDR_PREFIX+each.getAddress());
        });
        searchRelComments(commentDbEntityList);
        return commentDbEntityList;
    }

    private void searchRelComments(List<CommentDbEntity> commentDbEntityList){
        if(CollectionUtils.isEmpty(commentDbEntityList)){
            return ;
        }
        for(CommentDbEntity commentDbEntity : commentDbEntityList){
            List<CommentDbEntity> replayComments = commentDao.queryByEntityIdAndType(commentDbEntity.getId(),EntityTypeEnum.ENTITY_TYPE_COMMENT.getCode());
            if(CollectionUtils.isEmpty(replayComments)){
                continue;
            }
            //敏感信息进行脱敏
            replayComments.stream().forEach(each->{
                each.setContent(sensitiveWordsHandler.filter(each.getContent()));
                each.setAddress(Constant.BConstants.ADDR_PREFIX+each.getAddress());
            });
            commentDbEntity.setReplays(replayComments);
            searchRelComments(replayComments);
        }
    }

}
