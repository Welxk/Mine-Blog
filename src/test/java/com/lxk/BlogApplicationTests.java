package com.lxk;

import com.lxk.dao.UserDbDao;
import com.lxk.domain.UserDbEntity;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BlogApplication.class})
class BlogApplicationTests {

	@Autowired
	private UserDbDao userDbDao;

	@Test
	public void testSelect(){
		UserDbEntity user = userDbDao.checkUser("lxk","lxk");
		System.out.println(user);
	}


	@Test
	void contextLoads() {

	}

}
